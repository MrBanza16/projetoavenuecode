########################################################
########### Scenario 1 - Create a valid task   #########
########################################################
Given (/^I am on ToDo App main screen$/) do
	main = AutomationMainPage.new
	visit DATAS["utilities"]["url"]
    main.clickOnSignIn		
end

When (/^sign in a valid user$/) do
    login = AutomationSignInPage.new
    login.selectEmail
    login.fullfillEmail(DATAS['utilities']['user'])
    login.selectPassword
    login.fullfillPassword(DATAS['utilities']['pass'])
    login.pressSignIn
end 

And (/^access My tasks menu$/) do
    access = AutomationMainPage.new
    access.clicklinkMyTasks
end

And (/^I am allowed to add a new task$/) do
    cadastrate = AutomationMyTaskPage.new
    cadastrate.clickOnNewTask
    cadastrate.fullfillNewTask
end

Then (/^a new task must been created successfully$/) do
    result = AutomationMyTaskPage.new
    result.validateCreatedTask
    result.cleanLastOperation
end


########################################################33#
########### Scenario 2 - Create a valid subtask   #########
##########################################################3
Given (/^I access My task menu$/) do
    main = AutomationMainPage.new
    visit DATAS["utilities"]["url"]
    main.clickOnSignIn
    login = AutomationSignInPage.new
    login.selectEmail
    login.fullfillEmail(DATAS['utilities']['user'])
    login.selectPassword
    login.fullfillPassword(DATAS['utilities']['pass'])
    login.pressSignIn
    main.clicklinkMyTasks
end

When (/^I select a created task$/) do
    cadastrate = AutomationMyTaskPage.new
    cadastrate.clickOnNewTask
    cadastrate.fullfillNewTask
end 

And (/^select manage subtask$/) do
    choice = AutomationMyTaskPage.new
    choice.selectManageSubTask
end

And (/^I am allowed to add a new subtask$/) do
   choice = AutomationMyTaskPage.new
   choice.clickSubTaskDescription
   choice.fullfillSubTaskDescription
   choice.clickDuoDate
   choice.fullfillDuoDate
end

Then (/^a new subtask must been created successfully$/) do
    result = AutomationMyTaskPage.new
    result.validateCreatedSubTask
    result.cleanSubTask
    result.ClickClose
    result.cleanLastOperation
end