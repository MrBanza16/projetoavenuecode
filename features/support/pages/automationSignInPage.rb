class AutomationSignInPage < SitePrism::Page

    element :barEmail, :xpath, "//*[@id='user_email']"
    element :barPassword, :xpath, "//*[@id='user_password']"
    element :btnSignIn, :xpath, "//*[@class='btn btn-primary']"
    
    
    def selectEmail
        barEmail.click 
    end

    def fullfillEmail(validEmail)
        barEmail.set(validEmail)
    end

    def selectPassword
        barPassword.click 
    end

    def fullfillPassword(validPassword)
        barPassword.set(validPassword)
    end

    def pressSignIn
        btnSignIn.click
    end
end