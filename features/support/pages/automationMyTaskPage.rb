class AutomationMyTaskPage < SitePrism::Page

    element :barNewTask, :xpath, "(//*[@id='new_task'])"
    element :btnManageSubtask, :xpath, "//*[@class='btn btn-xs btn-primary ng-binding']"
    element :btnRemove, :xpath, "//*[@class='btn btn-xs btn-danger']"

    element :barSubtaskDesc, :xpath, "//*[@id='new_sub_task']"
    element :barDueDate, :xpath, "//*[@id='dueDate']"
    element :btnRemoveSubTask, :xpath, "/html/body/div[4]/div/div/div[2]/div[2]/table/tbody/tr/td[3]/button"

    element :btnclose, :xpath, "(//*[@class='btn btn-primary'])[2]"
    
    def clickOnNewTask
        barNewTask.click
    end

    def fullfillNewTask
        require 'win32ole'
        wsh = WIN32OLE.new('Wscript.shell')
        barNewTask.set("The show must go on")
        wsh.SendKeys "{ENTER}"
    end

    def validateCreatedTask
        assert_text("The show must go on")
    end

    def cleanLastOperation
        btnRemove.click
    end

    def selectManageSubTask
        btnManageSubtask.click
    end

    def clickSubTaskDescription
        barSubtaskDesc.click
    end

    def fullfillSubTaskDescription
        barSubtaskDesc.set("All around the Globe")
    end

    def clickDuoDate
        barDueDate.click
    end

    def fullfillDuoDate
        require 'win32ole'
        wsh = WIN32OLE.new('Wscript.shell')
        barDueDate.set("04/16/2018")
        wsh.SendKeys "{ENTER}"
    end

     def validateCreatedSubTask
        assert_text("All around the Globe")
    end

    def cleanSubTask
        btnRemoveSubTask.click
    end

    def ClickClose
        btnclose.click
    end

end