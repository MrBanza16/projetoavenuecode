class AutomationMainPage < SitePrism::Page

    element :linkSignIn, :xpath, "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[1]/a"
    element :linkMyTasks, :xpath, "/html/body/div[1]/div[1]/div/div[2]/ul[1]/li[2]/a"
    
    
    def clicklinkMyTasks
        linkMyTasks.click 
    end

    def clickOnSignIn
    	linkSignIn.click
    end
end