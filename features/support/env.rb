require 'rspec'
require 'cucumber'
require 'selenium-webdriver'
require 'capybara'
require 'capybara/cucumber'
require 'rspec/autorun'
require 'capybara/rspec'
require 'pry'
require 'cucumber_characteristics/autoload'
require 'fileutils'
require 'site_prism'
require 'spreadsheet'
require 'roo'
require 'net/http'

ENV_TYPE = ENV['ENV_TYPE']
BROWSER = "chrome"

RSpec.configure do |config|
  config.include Capybara::DSL, :type => :request
end

Capybara.register_driver :selenium do |app|
	if BROWSER.eql?('chrome')
		
		Capybara::Selenium::Driver.new(
			app, 
			:browser => :chrome,
			:desired_capabilities => Selenium::WebDriver::Remote::Capabilities.chrome(
				'chromeOptions' => {
					'args' => [
					"start-maximized",
					"--no-sandbox",
                	"--disable-web-security",
                	"--disable-extensions",
                	"--allow-running-insecure-content"],
                	useAutomationExtension: false,
				}
			)
		)
	elsif BROWSER.eql?('firefox')
		
		Capybara::Selenium::Driver.new(
			app, 
			:browser => :firefox,
			:marionette => true
		)
	elsif BROWSER.eql?('ie')
		
		Capybara::Selenium::Driver.new(
			app, 
			:browser => :internet_explorer	
		)
	end	
end

Capybara.default_driver = :selenium
Capybara.default_max_wait_time = 40

ENV['ENV'] = 'HK' unless ENV.key?'ENV'
DATAS = YAML.load_file('./features/Global_data.yml')[ENV['ENV']]