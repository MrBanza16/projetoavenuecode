
@automationQA
Feature: Creation of task and subtask operation
	As a user 
	I'd like to create a task and subtask
	so that is possible to add a new task and subtask

@CreateTask
Scenario: Create a valid task
	Given I am on ToDo App main screen
	When sign in a valid user
	And access My tasks menu
	And I am allowed to add a new task
	Then a new task must been created successfully

@CreateSubTask
Scenario: Create a valid subtask
	Given I access My task menu
	When I select a created task
	And select manage subtask
	And I am allowed to add a new subtask
	Then a new subtask must been created successfully